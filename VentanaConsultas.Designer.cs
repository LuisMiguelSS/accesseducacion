﻿namespace AccessEducacion
{
    partial class VentanaConsultas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaConsultas));
            this.consultaDataGridView = new System.Windows.Forms.DataGridView();
            this.labelError = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.consultaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // consultaDataGridView
            // 
            this.consultaDataGridView.AllowUserToAddRows = false;
            this.consultaDataGridView.AllowUserToDeleteRows = false;
            this.consultaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.consultaDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.consultaDataGridView.Location = new System.Drawing.Point(0, 0);
            this.consultaDataGridView.Name = "consultaDataGridView";
            this.consultaDataGridView.ReadOnly = true;
            this.consultaDataGridView.Size = new System.Drawing.Size(400, 202);
            this.consultaDataGridView.TabIndex = 0;
            // 
            // labelError
            // 
            this.labelError.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelError.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelError.Location = new System.Drawing.Point(100, 64);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(200, 80);
            this.labelError.TabIndex = 1;
            this.labelError.Text = "Ha ocurrido un error tratando de cargar los datos de la consulta";
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // VentanaConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 202);
            this.Controls.Add(this.consultaDataGridView);
            this.Controls.Add(this.labelError);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VentanaConsultas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Consultas: BD Educación";
            ((System.ComponentModel.ISupportInitialize)(this.consultaDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView consultaDataGridView;
        private System.Windows.Forms.Label labelError;
    }
}