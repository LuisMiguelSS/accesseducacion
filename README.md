﻿# Access Educación <img src="resources/school.png" width="35px">

### Funcionalidades
 - [x] Lectura de datos sobre una base de datos en Access
 - [x] Modificación de valores
 - [x] Añadir y eliminar valores
 - [x] Consultas fijas:
   * Profesores del departamento de Lengua
   * Alumnos mayores de 21 años
 - [x] Cambio de tipo de vista:
   * Lectura
   * Edición
   * Ambas
 - [x] Interfaz adaptable a distintos tamaños de pantalla aguantando la proporción.

---
### Otras notas
El proyecto funciona correctamente. No obstante, los cambios sólo serán persistentes en el ejecutable proporcionado (también disponible en ```./bin/Debug/```), debido a que así ha sido establecido como resultado de la vinculación de la base de datos como copia para su correcto uso en otros equipos.

---
Para más detalles y visualización del control de versiones, ver [aquí](https://bitbucket.org/LuisMiguelSS/accesseducacion/src/master/).
Proyecto realizado por [@LuismiguelSS](https://luismiguelss.github.io/me).