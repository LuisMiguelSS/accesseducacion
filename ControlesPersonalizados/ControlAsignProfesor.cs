﻿using System.Data;
using System.Windows.Forms;

namespace AccessEducacion
{
    public partial class ControlAsignProfesor : UserControl
    {
        //
        // Atributos
        //
        private readonly ConsultaAvanzada VentanaConsultas;
        private readonly DataTable Tabla;

        //
        // Constructores
        //
        private ControlAsignProfesor() : this(null, null)
        {
            // Está privado para no permitir ser usado desde fuera y únicamente
            // por si posteriormente se le añade alguna otra funcionalidad.
        }
        public ControlAsignProfesor(ConsultaAvanzada panelConsultas, DataTable tabla)
        {
            InitializeComponent();

            Dock = DockStyle.Fill;
            VentanaConsultas = panelConsultas;
            Tabla = tabla;

            CargarAsignaturasEnDeplegable();
        }

        //
        // Otros métodos
        //
        public void CargarAsignaturasEnDeplegable()
        {
            if(Tabla != null)
            {
                // Obtener nombres de asignaturas
                object[] asignaturas = new object[Tabla.Rows.Count];

                for (int i = 0; i < Tabla.Rows.Count; i++)
                    asignaturas[i] = Tabla.Rows[i]["Nombre"].ToString();

                desplegableAsignaturas.Items.AddRange(asignaturas);
            }
        }
        public int GetIdAsignaturaSeleccionada()
        {
            if(Tabla != null)
            {
                // Iterar por asignaturas
                for (int i = 0; i < Tabla.Rows.Count; i++)
                    if (desplegableAsignaturas.SelectedItem.ToString() == Tabla.Rows[i]["Nombre"].ToString())
                        return (int)Tabla.Rows[i]["Id"];

            }

            return -1;
        }

        private void DesplegableAsignaturas_SelectedValueChanged(object sender, System.EventArgs e)
        {
            VentanaConsultas.FiltrarProfesoresPorAsignatura(GetIdAsignaturaSeleccionada());
        }
    }
}
