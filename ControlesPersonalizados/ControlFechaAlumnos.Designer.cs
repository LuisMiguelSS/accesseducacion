﻿namespace AccessEducacion.ControlesPersonalizados
{
    partial class ControlFechaAlumnos
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.fechaMin = new System.Windows.Forms.DateTimePicker();
            this.labelEntre = new System.Windows.Forms.Label();
            this.labelY = new System.Windows.Forms.Label();
            this.fechaMax = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // fechaMin
            // 
            this.fechaMin.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fechaMin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaMin.Location = new System.Drawing.Point(43, 3);
            this.fechaMin.Name = "fechaMin";
            this.fechaMin.Size = new System.Drawing.Size(79, 20);
            this.fechaMin.TabIndex = 0;
            this.fechaMin.Value = new System.DateTime(2019, 12, 15, 0, 0, 0, 0);
            this.fechaMin.ValueChanged += new System.EventHandler(this.fechaMax_ValueChanged);
            // 
            // labelEntre
            // 
            this.labelEntre.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelEntre.AutoSize = true;
            this.labelEntre.Location = new System.Drawing.Point(4, 6);
            this.labelEntre.Name = "labelEntre";
            this.labelEntre.Size = new System.Drawing.Size(35, 13);
            this.labelEntre.TabIndex = 1;
            this.labelEntre.Text = "Entre:";
            // 
            // labelY
            // 
            this.labelY.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(132, 5);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(12, 13);
            this.labelY.TabIndex = 2;
            this.labelY.Text = "y";
            // 
            // fechaMax
            // 
            this.fechaMax.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fechaMax.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fechaMax.Location = new System.Drawing.Point(154, 3);
            this.fechaMax.Name = "fechaMax";
            this.fechaMax.Size = new System.Drawing.Size(79, 20);
            this.fechaMax.TabIndex = 3;
            this.fechaMax.ValueChanged += new System.EventHandler(this.fechaMax_ValueChanged);
            // 
            // ControlFechaAlumnos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fechaMax);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.labelEntre);
            this.Controls.Add(this.fechaMin);
            this.Name = "ControlFechaAlumnos";
            this.Size = new System.Drawing.Size(238, 27);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker fechaMin;
        private System.Windows.Forms.Label labelEntre;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.DateTimePicker fechaMax;
    }
}
