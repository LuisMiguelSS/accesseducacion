﻿using System;
using System.Windows.Forms;

namespace AccessEducacion.ControlesPersonalizados
{
    public partial class ControlFechaAlumnos : UserControl
    {
        //
        // Atributos
        //
        private ConsultaAvanzada panelConsultas;
        //
        // Constructor
        //
        private ControlFechaAlumnos() : this(null)
        {
            // Está privado para no permitir ser usado desde fuera y únicamente
            // por si posteriormente se le añade alguna otra funcionalidad.
        }
        public ControlFechaAlumnos(ConsultaAvanzada panelConsultas)
        {
            InitializeComponent();

            this.panelConsultas = panelConsultas;
            Dock = DockStyle.Fill;

            // Fechas límite
            fechaMin.MaxDate = DateTime.Today;
            fechaMax.MaxDate = DateTime.Today;
        }

        private void fechaMax_ValueChanged(object sender, EventArgs e)
        {
            panelConsultas.FiltrarAlumnosPorFechas(fechaMin.Value, fechaMax.Value);
        }
    }
}
