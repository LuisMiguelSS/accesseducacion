﻿namespace AccessEducacion
{
    partial class ControlAsignProfesor
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelPrincipal = new System.Windows.Forms.Label();
            this.desplegableAsignaturas = new System.Windows.Forms.ComboBox();
            this.asignaturasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.educacionDataSet = new AccessEducacion.educacionDataSet();
            this.profesoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.profesoresTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.ProfesoresTableAdapter();
            this.asignaturasTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.AsignaturasTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.educacionDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // labelPrincipal
            // 
            this.labelPrincipal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.labelPrincipal.AutoSize = true;
            this.labelPrincipal.Location = new System.Drawing.Point(4, 5);
            this.labelPrincipal.Name = "labelPrincipal";
            this.labelPrincipal.Size = new System.Drawing.Size(60, 13);
            this.labelPrincipal.TabIndex = 0;
            this.labelPrincipal.Text = "Asignatura:";
            // 
            // desplegableAsignaturas
            // 
            this.desplegableAsignaturas.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.desplegableAsignaturas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.desplegableAsignaturas.FormattingEnabled = true;
            this.desplegableAsignaturas.Location = new System.Drawing.Point(68, 2);
            this.desplegableAsignaturas.Name = "desplegableAsignaturas";
            this.desplegableAsignaturas.Size = new System.Drawing.Size(121, 21);
            this.desplegableAsignaturas.TabIndex = 1;
            this.desplegableAsignaturas.SelectedValueChanged += new System.EventHandler(this.DesplegableAsignaturas_SelectedValueChanged);
            // 
            // asignaturasBindingSource
            // 
            this.asignaturasBindingSource.DataMember = "Asignaturas";
            this.asignaturasBindingSource.DataSource = this.educacionDataSet;
            // 
            // educacionDataSet
            // 
            this.educacionDataSet.DataSetName = "educacionDataSet";
            this.educacionDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // profesoresBindingSource
            // 
            this.profesoresBindingSource.DataMember = "Profesores";
            this.profesoresBindingSource.DataSource = this.educacionDataSet;
            // 
            // profesoresTableAdapter
            // 
            this.profesoresTableAdapter.ClearBeforeFill = true;
            // 
            // asignaturasTableAdapter
            // 
            this.asignaturasTableAdapter.ClearBeforeFill = true;
            // 
            // ControlAsignProfesor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.desplegableAsignaturas);
            this.Controls.Add(this.labelPrincipal);
            this.Name = "ControlAsignProfesor";
            this.Size = new System.Drawing.Size(192, 25);
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.educacionDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPrincipal;
        private System.Windows.Forms.ComboBox desplegableAsignaturas;
        private System.Windows.Forms.BindingSource profesoresBindingSource;
        private educacionDataSet educacionDataSet;
        private educacionDataSetTableAdapters.ProfesoresTableAdapter profesoresTableAdapter;
        private System.Windows.Forms.BindingSource asignaturasBindingSource;
        private educacionDataSetTableAdapters.AsignaturasTableAdapter asignaturasTableAdapter;
    }
}
