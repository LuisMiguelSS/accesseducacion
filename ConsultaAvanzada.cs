﻿using AccessEducacion.ControlesPersonalizados;
using System;
using System.Data;
using System.Windows.Forms;

namespace AccessEducacion
{
    public partial class ConsultaAvanzada : Form
    {
        //
        // Atributos
        //
        private readonly educacionDataSet dataSet;

        //
        // Constructores
        //
        private ConsultaAvanzada() : this(null)
        {
            // Está privado para no permitir ser usado desde fuera y únicamente
            // por si posteriormente se le añade alguna otra funcionalidad.
        }
        public ConsultaAvanzada(educacionDataSet dataSet)
        {
            InitializeComponent();

            this.dataSet = dataSet;

        }

        //
        // Otros métodos
        //
        public void CargarOpcionesFiltradoAlumno()
        {
            // Cargar opciones secundarias
            GrupoOpcionesSecundarias.Enabled = true;
            GrupoOpcionesSecundarias.Controls.Clear();

            GrupoOpcionesSecundarias.Controls.Add(new ControlFechaAlumnos(this));

            // Cargar tabla
            resultadoDataGridView.DataSource = dataSet.Tables["Alumnos"];
        }
        public void CargarOpcionesFiltradoProfesor()
        {
            // Cargar opciones secundarias
            GrupoOpcionesSecundarias.Enabled = true;
            GrupoOpcionesSecundarias.Controls.Clear();

            GrupoOpcionesSecundarias.Controls.Add(new ControlAsignProfesor(this, dataSet.Tables["Asignaturas"]));

            // Cargar tabla
            resultadoDataGridView.DataSource = dataSet.Tables["Profesores"];
        }

        public void FiltrarAlumnosPorFechas(DateTime fechaMin, DateTime fechaMax)
        {
            if (fechaMin < fechaMax)
            {

                DataTable tablaResultado = new DataTable();
                tablaResultado.Columns.Add("Nombre", typeof(string));
                tablaResultado.Columns.Add("Dirección", typeof(string));

                DataRow[] rows = new DataRow[dataSet.Tables["Alumnos"].Rows.Count];

                // Iterar sobre las filas para buscar los resultados
                for (int i = 0; i < rows.Length; i++)
                {

                    object o = dataSet.Tables["Alumnos"].Rows[i]["FechaNacim"];

                    // Comprobar que el campo de fecha sea válido
                    if (o != DBNull.Value)
                    {
                        DateTime fechaNacim = (DateTime)o;
                        if (fechaNacim.Ticks >= fechaMin.Ticks && fechaNacim.Ticks <= fechaMax.Ticks)
                        {
                            object[] values = new object[] { dataSet.Tables["Alumnos"].Rows[i]["Nombre"],
                                                             dataSet.Tables["Alumnos"].Rows[i]["Direccion"]
                                              };
                            tablaResultado.Rows.Add(values);

                        }
                    }

                }

                resultadoDataGridView.DataSource = tablaResultado;

            }
        }

        public void FiltrarProfesoresPorAsignatura(int IdAsignatura = -1)
        {
            if(IdAsignatura != -1)
            {
                DataTable tablaResultado = new DataTable();
                tablaResultado.Columns.Add("Nombre", typeof(string));
                tablaResultado.Columns.Add("Departamento", typeof(string));

                DataRow[] rows = new DataRow[dataSet.Tables["Profesores"].Rows.Count];

                // Iterar sobre las asignaturas para obtener el DNI del profesor
                for (int i = 0; i < rows.Length; i++)
                {
                    string dniProfesor = GetDniProfesorPorIdAsignatura(IdAsignatura);

                    object o = dataSet.Tables["Profesores"].Rows[i]["DNI"];

                    // Comprobar que sea válido
                    if (o != DBNull.Value)
                    {
                        string dniParseado = o.ToString();
                        if (dniParseado == dniProfesor)
                        {
                            object[] values = new object[] { dataSet.Tables["Profesores"].Rows[i]["Nombre"],
                                                             GetNombreDepartamentoById( (int)dataSet.Tables["Profesores"].Rows[i]["Id_Departamento"])
                                              };
                            tablaResultado.Rows.Add(values);

                        }
                    }

                }

                resultadoDataGridView.DataSource = tablaResultado;
            }
        }
        public string GetDniProfesorPorIdAsignatura(int idAsig = -1)
        {
            if (idAsig > -1)
            {
                for (int i = 0; i < dataSet.Tables["Asignaturas"].Rows.Count; i++)
                    if ((int)dataSet.Tables["Asignaturas"].Rows[i]["Id"] == idAsig)
                        return dataSet.Tables["Asignaturas"].Rows[i]["DNI_Profesor"].ToString();
            }
            return "";
        }
        public string GetNombreDepartamentoById(int id = -1)
        {
            if(id > -1)
            {
                for(int i = 0; i < dataSet.Tables["Departamentos"].Rows.Count; i++)
                    if ((int)dataSet.Tables["Departamentos"].Rows[i]["Id"] == id)
                        return dataSet.Tables["Departamentos"].Rows[i]["Nombre"].ToString();
            }
            return "";
        }

        //
        // Listeners
        //
        private void RadioAlumnoProfesor_CheckedChanged(object sender, EventArgs e)
        {
            if (RadioAlumno.Checked)
                CargarOpcionesFiltradoAlumno();
            else
                CargarOpcionesFiltradoProfesor();
        }

    }
}
