﻿namespace AccessEducacion
{
    partial class ConsultaAvanzada
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConsultaAvanzada));
            this.PanelTablaPrincipal = new System.Windows.Forms.TableLayoutPanel();
            this.GrupoOpcionesPrincipal = new System.Windows.Forms.GroupBox();
            this.RadioProfesor = new System.Windows.Forms.RadioButton();
            this.RadioAlumno = new System.Windows.Forms.RadioButton();
            this.GrupoOpcionesSecundarias = new System.Windows.Forms.GroupBox();
            this.resultadoDataGridView = new System.Windows.Forms.DataGridView();
            this.PanelTablaPrincipal.SuspendLayout();
            this.GrupoOpcionesPrincipal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultadoDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // PanelTablaPrincipal
            // 
            this.PanelTablaPrincipal.ColumnCount = 1;
            this.PanelTablaPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.PanelTablaPrincipal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.PanelTablaPrincipal.Controls.Add(this.resultadoDataGridView, 0, 2);
            this.PanelTablaPrincipal.Controls.Add(this.GrupoOpcionesPrincipal, 0, 0);
            this.PanelTablaPrincipal.Controls.Add(this.GrupoOpcionesSecundarias, 0, 1);
            this.PanelTablaPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelTablaPrincipal.Location = new System.Drawing.Point(0, 0);
            this.PanelTablaPrincipal.Name = "PanelTablaPrincipal";
            this.PanelTablaPrincipal.RowCount = 3;
            this.PanelTablaPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.47761F));
            this.PanelTablaPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.52239F));
            this.PanelTablaPrincipal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.PanelTablaPrincipal.Size = new System.Drawing.Size(455, 354);
            this.PanelTablaPrincipal.TabIndex = 0;
            // 
            // GrupoOpcionesPrincipal
            // 
            this.GrupoOpcionesPrincipal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GrupoOpcionesPrincipal.Controls.Add(this.RadioProfesor);
            this.GrupoOpcionesPrincipal.Controls.Add(this.RadioAlumno);
            this.GrupoOpcionesPrincipal.Location = new System.Drawing.Point(99, 22);
            this.GrupoOpcionesPrincipal.Margin = new System.Windows.Forms.Padding(3, 10, 3, 3);
            this.GrupoOpcionesPrincipal.Name = "GrupoOpcionesPrincipal";
            this.GrupoOpcionesPrincipal.Size = new System.Drawing.Size(257, 57);
            this.GrupoOpcionesPrincipal.TabIndex = 0;
            this.GrupoOpcionesPrincipal.TabStop = false;
            this.GrupoOpcionesPrincipal.Text = "Elección de datos";
            // 
            // RadioProfesor
            // 
            this.RadioProfesor.AutoSize = true;
            this.RadioProfesor.Location = new System.Drawing.Point(148, 20);
            this.RadioProfesor.Name = "RadioProfesor";
            this.RadioProfesor.Size = new System.Drawing.Size(75, 17);
            this.RadioProfesor.TabIndex = 1;
            this.RadioProfesor.Text = "Profesores";
            this.RadioProfesor.UseVisualStyleBackColor = true;
            this.RadioProfesor.CheckedChanged += new System.EventHandler(this.RadioAlumnoProfesor_CheckedChanged);
            // 
            // RadioAlumno
            // 
            this.RadioAlumno.AutoSize = true;
            this.RadioAlumno.Location = new System.Drawing.Point(24, 20);
            this.RadioAlumno.Name = "RadioAlumno";
            this.RadioAlumno.Size = new System.Drawing.Size(65, 17);
            this.RadioAlumno.TabIndex = 0;
            this.RadioAlumno.Text = "Alumnos";
            this.RadioAlumno.UseVisualStyleBackColor = true;
            this.RadioAlumno.CheckedChanged += new System.EventHandler(this.RadioAlumnoProfesor_CheckedChanged);
            // 
            // GrupoOpcionesSecundarias
            // 
            this.GrupoOpcionesSecundarias.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GrupoOpcionesSecundarias.Enabled = false;
            this.GrupoOpcionesSecundarias.Location = new System.Drawing.Point(69, 97);
            this.GrupoOpcionesSecundarias.Name = "GrupoOpcionesSecundarias";
            this.GrupoOpcionesSecundarias.Size = new System.Drawing.Size(316, 72);
            this.GrupoOpcionesSecundarias.TabIndex = 1;
            this.GrupoOpcionesSecundarias.TabStop = false;
            this.GrupoOpcionesSecundarias.Text = "Filtrado";
            // 
            // resultadoDataGridView
            // 
            this.resultadoDataGridView.AllowUserToAddRows = false;
            this.resultadoDataGridView.AllowUserToDeleteRows = false;
            this.resultadoDataGridView.AllowUserToOrderColumns = true;
            this.resultadoDataGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.resultadoDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.resultadoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultadoDataGridView.Enabled = false;
            this.resultadoDataGridView.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.resultadoDataGridView.Location = new System.Drawing.Point(38, 180);
            this.resultadoDataGridView.Name = "resultadoDataGridView";
            this.resultadoDataGridView.ReadOnly = true;
            this.resultadoDataGridView.Size = new System.Drawing.Size(378, 166);
            this.resultadoDataGridView.TabIndex = 2;
            // 
            // ConsultaAvanzada
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 354);
            this.Controls.Add(this.PanelTablaPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ConsultaAvanzada";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Consulta específica de datos: BD Educación";
            this.PanelTablaPrincipal.ResumeLayout(false);
            this.GrupoOpcionesPrincipal.ResumeLayout(false);
            this.GrupoOpcionesPrincipal.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resultadoDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel PanelTablaPrincipal;
        private System.Windows.Forms.GroupBox GrupoOpcionesPrincipal;
        private System.Windows.Forms.RadioButton RadioProfesor;
        private System.Windows.Forms.RadioButton RadioAlumno;
        private System.Windows.Forms.GroupBox GrupoOpcionesSecundarias;
        private System.Windows.Forms.DataGridView resultadoDataGridView;
    }
}