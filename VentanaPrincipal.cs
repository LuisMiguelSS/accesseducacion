﻿using System;
using System.Data;
using System.Windows.Forms;

namespace AccessEducacion
{
    public partial class VentanaPrincipal : Form
    {
        //
        // Atributos
        //
        public enum Vista
        {
            Tabla, Edicion, Ambas
        }

        //
        // Constructor
        //
        public VentanaPrincipal()
        {
            InitializeComponent();

            fechaNacimDateTimePicker.MaxDate = DateTime.Today;
        }

        //
        // Otros métodos
        //
        public void Guardar()
        {
            Validate();

            try
            {
                alumnosBindingSource.EndEdit();
                asignaturasBindingSource.EndEdit();
                departamentosBindingSource.EndEdit();
                profesoresBindingSource.EndEdit();

                // Contadores
                ActualizarContadorAlumno();
                ActualizarContadorAsignatura();
                ActualizarContadorDepartamento();
                ActualizarContadorProfesor();

                // Actualizar tablas
                alumnosTableAdapter.Update(educacionDataSet);
                asignaturasTableAdapter.Update(educacionDataSet);
                departamentosTableAdapter.Update(educacionDataSet);
                profesoresTableAdapter.Update(educacionDataSet);

                if (educacionDataSet.HasChanges())
                    educacionDataSet.AcceptChanges();
            }
            catch(DBConcurrencyException)
            {
                MessageBox.Show("No se ha podido actualizar la información.\n\nEsto es probablemente debido al intento de modificación de un campo identificativo especial o con fuertes referencias a otros.", "Error en la actualización", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Fallo en la actualización", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

        }

        // TODO: añadir todos los datagridview que quedan por hacer.
        public void CambiarVistaDatos(Vista v)
        {

            switch (v)
            {
                case Vista.Edicion: // Edición

                    // Alumnos
                    DistribucionTablaAlumnos.ColumnStyles[0].SizeType = SizeType.Absolute;
                    DistribucionTablaAlumnos.ColumnStyles[0].Width = 0;

                    // Asignaturas
                    DistribucionTablaAsignaturas.ColumnStyles[0].SizeType = SizeType.Absolute;
                    DistribucionTablaAsignaturas.ColumnStyles[0].Width = 0;

                    // Departamentos
                    DistribucionTablaDepartamentos.ColumnStyles[0].SizeType = SizeType.Absolute;
                    DistribucionTablaDepartamentos.ColumnStyles[0].Width = 0;

                    // Profesores
                    DistribucionTablaProfesores.ColumnStyles[0].SizeType = SizeType.Absolute;
                    DistribucionTablaProfesores.ColumnStyles[0].Width = 0;
                    break;

                case Vista.Tabla: // Tabla

                    // Alumnos
                    DistribucionTablaAlumnos.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaAlumnos.ColumnStyles[0].Width = 100f;

                    DistribucionTablaAlumnos.ColumnStyles[1].SizeType = SizeType.Absolute;
                    DistribucionTablaAlumnos.ColumnStyles[1].Width = 0;

                    // Asignaturas
                    DistribucionTablaAsignaturas.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaAsignaturas.ColumnStyles[0].Width = 100f;

                    DistribucionTablaAsignaturas.ColumnStyles[1].SizeType = SizeType.Absolute;
                    DistribucionTablaAsignaturas.ColumnStyles[1].Width = 0;

                    // Departamentos
                    DistribucionTablaDepartamentos.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaDepartamentos.ColumnStyles[0].Width = 100f;

                    DistribucionTablaDepartamentos.ColumnStyles[1].SizeType = SizeType.Absolute;
                    DistribucionTablaDepartamentos.ColumnStyles[1].Width = 0;

                    // Profesores
                    DistribucionTablaProfesores.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaProfesores.ColumnStyles[0].Width = 100f;

                    DistribucionTablaProfesores.ColumnStyles[1].SizeType = SizeType.Absolute;
                    DistribucionTablaProfesores.ColumnStyles[1].Width = 0;
                    break;

                default: // Ambas

                    // Alumnos
                    DistribucionTablaAlumnos.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaAlumnos.ColumnStyles[0].Width = 42.98f;

                    DistribucionTablaAlumnos.ColumnStyles[1].SizeType = SizeType.Percent;
                    DistribucionTablaAlumnos.ColumnStyles[1].Width = 57.02f;

                    // Asignaturas
                    DistribucionTablaAsignaturas.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaAsignaturas.ColumnStyles[0].Width = 42.98f;

                    DistribucionTablaAsignaturas.ColumnStyles[1].SizeType = SizeType.Percent;
                    DistribucionTablaAsignaturas.ColumnStyles[1].Width = 57.02f;

                    // Departamentos
                    DistribucionTablaDepartamentos.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaDepartamentos.ColumnStyles[0].Width = 42.98f;

                    DistribucionTablaDepartamentos.ColumnStyles[1].SizeType = SizeType.Percent;
                    DistribucionTablaDepartamentos.ColumnStyles[1].Width = 57.02f;

                    // Profesores
                    DistribucionTablaProfesores.ColumnStyles[0].SizeType = SizeType.Percent;
                    DistribucionTablaProfesores.ColumnStyles[0].Width = 42.98f;

                    DistribucionTablaProfesores.ColumnStyles[1].SizeType = SizeType.Percent;
                    DistribucionTablaProfesores.ColumnStyles[1].Width = 57.02f;
                    break;
            }

        }
        public void ActualizarContadorAlumno()
        {
            labelNumeroAlumno.Text = "Alumno " + (alumnosBindingSource.Position + 1) + " de " + alumnosBindingSource.Count;
        }
        public void ActualizarContadorAsignatura()
        {
            labelNumeroAsignaturas.Text = "Asignatura " + (asignaturasBindingSource.Position + 1) + " de " + asignaturasBindingSource.Count;
        }
        public void ActualizarContadorDepartamento()
        {
            labelNumeroDepartamentos.Text = "Departamento " + (departamentosBindingSource.Position + 1) + " de " + departamentosBindingSource.Count;
        }
        public void ActualizarContadorProfesor()
        {
            labelNumeroProfesores.Text = "Profesor " + (profesoresBindingSource.Position + 1) + " de " + profesoresBindingSource.Count;
        }

        //
        // Listeners
        //

        //
        // Generales del Formulario
        private void VentanaPrincipal_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'educacionDataSet.ProfesoresDeptLengua' Puede moverla o quitarla según sea necesario.
            profesoresDeptLenguaTableAdapter.Fill(educacionDataSet.ProfesoresDeptLengua);
            profesoresTableAdapter.Fill(educacionDataSet.Profesores);
            departamentosTableAdapter.Fill(educacionDataSet.Departamentos);
            asignaturasTableAdapter.Fill(educacionDataSet.Asignaturas);
            alumnosTableAdapter.Fill(educacionDataSet.Alumnos);

            ActualizarContadorAlumno();
            ActualizarContadorAsignatura();
            ActualizarContadorDepartamento();
            ActualizarContadorProfesor();

        }

        private void VentanaPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea salir?", "Confirmación de salida", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void MenuItemGuardar_Click(object sender, EventArgs e)
        {
            Guardar();
        }

        private void SalirMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        // Menú
        private void MenuItemVistaTabla_Click(object sender, EventArgs e)
        {
            CambiarVistaDatos(Vista.Tabla);
            MenuItemVistaTabla.Checked = true;
            MenuItemVistaEdicion.Checked = false;
            MenuItemVistaAmbos.Checked = false;
        }

        private void MenuItemVistaEdicion_Click(object sender, EventArgs e)
        {
            CambiarVistaDatos(Vista.Edicion);
            MenuItemVistaEdicion.Checked = true;
            MenuItemVistaTabla.Checked = false;
            MenuItemVistaAmbos.Checked = false;
        }

        private void MenuItemVistaAmbos_Click(object sender, EventArgs e)
        {
            CambiarVistaDatos(Vista.Ambas);
            MenuItemVistaAmbos.Checked = true;
            MenuItemVistaTabla.Checked = false;
            MenuItemVistaEdicion.Checked = false;
        }

        // Consultas
        private void MenuItemAlumnosMayores21_Click(object sender, EventArgs e)
        {
            alumnosMayores21TableAdapter.Fill(educacionDataSet.AlumnosMayores21);
            new VentanaConsultas(alumnosMayores21BindingSource).Visible = true;
        }

        private void ConsultaProfesoresDeptLengua_Click(object sender, EventArgs e)
        {
            profesoresDeptLenguaTableAdapter.Fill(educacionDataSet.ProfesoresDeptLengua);
            new VentanaConsultas(profesoresDeptLenguaBindingSource).Visible = true;
        }

        //
        // Alumnos
        private void BtnAlumnoAnterior_Click(object sender, EventArgs e)
        {
            alumnosBindingSource.MovePrevious();
            ActualizarContadorAlumno();
        }

        private void BtnAlumnoSiguiente_Click(object sender, EventArgs e)
        {
            alumnosBindingSource.MoveNext();
            ActualizarContadorAlumno();
        }

        private void GridAlumnos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ActualizarContadorAlumno();
        }

        private void BtnAnadirAlumno_Click(object sender, EventArgs e)
        {
            alumnosBindingSource.AddNew();
            dNITextBox.Focus();
            ActualizarContadorAlumno();
        }

        private void BtnBorrarAlumno_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea eliminar a este alumno?\nEsta acción eliminará todas las referencias a él.", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                alumnosBindingSource.RemoveCurrent();

            ActualizarContadorAlumno();
        }

        private void BtnIrPrimerAlumno_Click(object sender, EventArgs e)
        {
            alumnosBindingSource.MoveFirst();
            ActualizarContadorAlumno();
        }

        private void BtnIrUltimoAlumno_Click(object sender, EventArgs e)
        {
            alumnosBindingSource.MoveLast();
            ActualizarContadorAlumno();
        }


        //
        // Asignaturas
        private void BtnAsignaturaAnterior_Click(object sender, EventArgs e)
        {
            asignaturasBindingSource.MovePrevious();
            ActualizarContadorAsignatura();
        }

        private void BtnAsignaturaSiguiente_Click(object sender, EventArgs e)
        {
            asignaturasBindingSource.MoveNext();
            ActualizarContadorAsignatura();
        }

        private void BtnIrPrimeraAsignatura_Click(object sender, EventArgs e)
        {
            asignaturasBindingSource.MoveFirst();
            ActualizarContadorAsignatura();
        }

        private void BtnIrUltimaAsignatura_Click(object sender, EventArgs e)
        {
            asignaturasBindingSource.MoveLast();
            ActualizarContadorAsignatura();
        }

        private void BtnAnadirAsignatura_Click(object sender, EventArgs e)
        {
            asignaturasBindingSource.AddNew();
            idTextBox.Focus();
            ActualizarContadorAsignatura();
        }

        private void BtnEliminarAsignatura_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea eliminar esta asignatura?\nEsta acción eliminará todas las referencias a ella.", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                asignaturasBindingSource.RemoveCurrent();

            ActualizarContadorAsignatura();
        }

        private void GirdAsignaturas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ActualizarContadorAsignatura();
        }

        //
        // Departamentos
        private void BtnIrPrimerDepartamento_Click(object sender, EventArgs e)
        {
            departamentosBindingSource.MoveFirst();
            ActualizarContadorDepartamento();
        }

        private void BtnAnteriorDepartamento_Click(object sender, EventArgs e)
        {
            departamentosBindingSource.MovePrevious();
            ActualizarContadorDepartamento();
        }

        private void btnSiguienteDepartamento_Click(object sender, EventArgs e)
        {
            departamentosBindingSource.MoveNext();
            ActualizarContadorDepartamento();
        }

        private void btnUltimoDepartamento_Click(object sender, EventArgs e)
        {
            departamentosBindingSource.MoveLast();
            ActualizarContadorDepartamento();
        }

        private void departamentosDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ActualizarContadorDepartamento();
        }

        private void btnAnadirDepartamento_Click(object sender, EventArgs e)
        {
            departamentosBindingSource.AddNew();
            idTextBoxDept.Focus();
            ActualizarContadorDepartamento();
        }

        private void btnEliminarDepartamento_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea eliminar este departamento?\nEsta acción eliminará todas las referencias a él.", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                departamentosBindingSource.RemoveCurrent();

            ActualizarContadorDepartamento();
        }

        //
        // Profesores
        private void BtnPrimerProfesor_Click(object sender, EventArgs e)
        {
            profesoresBindingSource.MoveFirst();
            ActualizarContadorProfesor();
        }

        private void BtnAnteriorProfesor_Click(object sender, EventArgs e)
        {
            profesoresBindingSource.MovePrevious();
            ActualizarContadorProfesor();
        }

        private void BtnSiguienteProfesor_Click(object sender, EventArgs e)
        {
            profesoresBindingSource.MoveNext();
            ActualizarContadorProfesor();
        }

        private void BtnUltimoProfesor_Click(object sender, EventArgs e)
        {
            profesoresBindingSource.MoveLast();
            ActualizarContadorProfesor();
        }

        private void BtnAnadirProfesor_Click(object sender, EventArgs e)
        {
            profesoresBindingSource.AddNew();
            dNITextBoxProf.Focus();
            ActualizarContadorProfesor();
        }

        private void BtnEliminarProfesor_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Está seguro de que desea eliminar a este profesor?\nEsta acción eliminará todas las referencias a él.", "Confirmación de borrado", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                profesoresBindingSource.RemoveCurrent();

            ActualizarContadorProfesor();
        }

        private void profesoresDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ActualizarContadorProfesor();
        }

        private void MenuItemConsultaEspecifica_Click(object sender, EventArgs e)
        {
            new ConsultaAvanzada(educacionDataSet).Visible = true;
        }
    }
}
