﻿namespace AccessEducacion
{
    partial class VentanaPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label dNILabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VentanaPrincipal));
            System.Windows.Forms.Label nombreLabel;
            System.Windows.Forms.Label direccionLabel;
            System.Windows.Forms.Label telefonoLabel;
            System.Windows.Forms.Label fechaNacimLabel;
            System.Windows.Forms.Label id_AsignaturaLabel;
            System.Windows.Forms.Label idLabel;
            System.Windows.Forms.Label nombreLabel1;
            System.Windows.Forms.Label dNI_ProfesorLabel;
            System.Windows.Forms.Label idLabel1;
            System.Windows.Forms.Label nombreLabel2;
            System.Windows.Forms.Label dNILabel1;
            System.Windows.Forms.Label nombreLabel3;
            System.Windows.Forms.Label id_DepartamentoLabel;
            this.toolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.PanelPestanas = new System.Windows.Forms.TabControl();
            this.tabAlumnos = new System.Windows.Forms.TabPage();
            this.DistribucionTablaAlumnos = new System.Windows.Forms.TableLayoutPanel();
            this.PanelEdicionAlumno = new System.Windows.Forms.Panel();
            this.btnUltimoAlumno = new System.Windows.Forms.Button();
            this.btnPrimerAlumno = new System.Windows.Forms.Button();
            this.BtnBorrarAlumno = new System.Windows.Forms.Button();
            this.BtnAnadirAlumno = new System.Windows.Forms.Button();
            this.labelNumeroAlumno = new System.Windows.Forms.Label();
            this.btnSiguienteAlumno = new System.Windows.Forms.Button();
            this.btnAnteriorAlumno = new System.Windows.Forms.Button();
            this.labelTituloAlumnos = new System.Windows.Forms.Label();
            this.dNITextBox = new System.Windows.Forms.TextBox();
            this.alumnosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.educacionDataSet = new AccessEducacion.educacionDataSet();
            this.nombreTextBox = new System.Windows.Forms.TextBox();
            this.direccionTextBox = new System.Windows.Forms.TextBox();
            this.telefonoTextBox = new System.Windows.Forms.TextBox();
            this.fechaNacimDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.id_AsignaturaTextBox = new System.Windows.Forms.TextBox();
            this.alumnosDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabAsignaturas = new System.Windows.Forms.TabPage();
            this.DistribucionTablaAsignaturas = new System.Windows.Forms.TableLayoutPanel();
            this.asignaturasDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.asignaturasBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.PanelEdicionAsignatura = new System.Windows.Forms.Panel();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.nombreTextBox1 = new System.Windows.Forms.TextBox();
            this.dNI_ProfesorTextBox = new System.Windows.Forms.TextBox();
            this.btnUltimaAsignatura = new System.Windows.Forms.Button();
            this.btnPrimeraAsignatura = new System.Windows.Forms.Button();
            this.btnEliminarAsignatura = new System.Windows.Forms.Button();
            this.btnAnadirAsignatura = new System.Windows.Forms.Button();
            this.labelNumeroAsignaturas = new System.Windows.Forms.Label();
            this.btnSiguienteAsignatura = new System.Windows.Forms.Button();
            this.btnAnteriorAsignatura = new System.Windows.Forms.Button();
            this.labelTituloAsignaturas = new System.Windows.Forms.Label();
            this.tabDepartamentos = new System.Windows.Forms.TabPage();
            this.DistribucionTablaDepartamentos = new System.Windows.Forms.TableLayoutPanel();
            this.PanelEdicionDepartamentos = new System.Windows.Forms.Panel();
            this.idTextBoxDept = new System.Windows.Forms.TextBox();
            this.departamentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nombreTextBox2 = new System.Windows.Forms.TextBox();
            this.btnUltimoDepartamento = new System.Windows.Forms.Button();
            this.btnPrimerDepartamento = new System.Windows.Forms.Button();
            this.btnEliminarDepartamento = new System.Windows.Forms.Button();
            this.btnAnadirDepartamento = new System.Windows.Forms.Button();
            this.labelNumeroDepartamentos = new System.Windows.Forms.Label();
            this.btnSiguienteDepartamento = new System.Windows.Forms.Button();
            this.btnAnteriorDepartamento = new System.Windows.Forms.Button();
            this.labelTituloDepartamentos = new System.Windows.Forms.Label();
            this.departamentosDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabProfesores = new System.Windows.Forms.TabPage();
            this.DistribucionTablaProfesores = new System.Windows.Forms.TableLayoutPanel();
            this.PanelEdicionProfesores = new System.Windows.Forms.Panel();
            this.dNITextBoxProf = new System.Windows.Forms.TextBox();
            this.profesoresBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.nombreTextBox3 = new System.Windows.Forms.TextBox();
            this.id_DepartamentoTextBox = new System.Windows.Forms.TextBox();
            this.BtnUltimoProfesor = new System.Windows.Forms.Button();
            this.BtnPrimerProfesor = new System.Windows.Forms.Button();
            this.BtnEliminarProfesor = new System.Windows.Forms.Button();
            this.BtnAnadirProfesor = new System.Windows.Forms.Button();
            this.labelNumeroProfesores = new System.Windows.Forms.Label();
            this.BtnSiguienteProfesor = new System.Windows.Forms.Button();
            this.BtnAnteriorProfesor = new System.Windows.Forms.Button();
            this.labelTituloProfesores = new System.Windows.Forms.Label();
            this.profesoresDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Id_Departamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.imageListPestanas = new System.Windows.Forms.ImageList(this.components);
            this.BarraHerramientas = new System.Windows.Forms.ToolStrip();
            this.BtnGuardar = new System.Windows.Forms.ToolStripButton();
            this.BarraMenu = new System.Windows.Forms.MenuStrip();
            this.MenuArchivo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemGuardar = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuConsultas = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemAlumnosMayores21 = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemProfesoresLengua = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemConsultaEspecifica = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuVista = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemVistaTabla = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemVistaEdicion = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuItemVistaAmbos = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosMayores21TableAdapter = new AccessEducacion.educacionDataSetTableAdapters.AlumnosMayores21TableAdapter();
            this.alumnosTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.AlumnosTableAdapter();
            this.tableAdapterManager = new AccessEducacion.educacionDataSetTableAdapters.TableAdapterManager();
            this.asignaturasTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.AsignaturasTableAdapter();
            this.alumnosMayores21BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.departamentosTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.DepartamentosTableAdapter();
            this.profesoresTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.ProfesoresTableAdapter();
            this.profesoresDeptLenguaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.profesoresDeptLenguaTableAdapter = new AccessEducacion.educacionDataSetTableAdapters.ProfesoresDeptLenguaTableAdapter();
            dNILabel = new System.Windows.Forms.Label();
            nombreLabel = new System.Windows.Forms.Label();
            direccionLabel = new System.Windows.Forms.Label();
            telefonoLabel = new System.Windows.Forms.Label();
            fechaNacimLabel = new System.Windows.Forms.Label();
            id_AsignaturaLabel = new System.Windows.Forms.Label();
            idLabel = new System.Windows.Forms.Label();
            nombreLabel1 = new System.Windows.Forms.Label();
            dNI_ProfesorLabel = new System.Windows.Forms.Label();
            idLabel1 = new System.Windows.Forms.Label();
            nombreLabel2 = new System.Windows.Forms.Label();
            dNILabel1 = new System.Windows.Forms.Label();
            nombreLabel3 = new System.Windows.Forms.Label();
            id_DepartamentoLabel = new System.Windows.Forms.Label();
            this.toolStripContainer1.ContentPanel.SuspendLayout();
            this.toolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.toolStripContainer1.SuspendLayout();
            this.PanelPestanas.SuspendLayout();
            this.tabAlumnos.SuspendLayout();
            this.DistribucionTablaAlumnos.SuspendLayout();
            this.PanelEdicionAlumno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.educacionDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosDataGridView)).BeginInit();
            this.tabAsignaturas.SuspendLayout();
            this.DistribucionTablaAsignaturas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasBindingSource)).BeginInit();
            this.PanelEdicionAsignatura.SuspendLayout();
            this.tabDepartamentos.SuspendLayout();
            this.DistribucionTablaDepartamentos.SuspendLayout();
            this.PanelEdicionDepartamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departamentosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.departamentosDataGridView)).BeginInit();
            this.tabProfesores.SuspendLayout();
            this.DistribucionTablaProfesores.SuspendLayout();
            this.PanelEdicionProfesores.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresDataGridView)).BeginInit();
            this.BarraHerramientas.SuspendLayout();
            this.BarraMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosMayores21BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresDeptLenguaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dNILabel
            // 
            resources.ApplyResources(dNILabel, "dNILabel");
            dNILabel.Name = "dNILabel";
            // 
            // toolStripContainer1
            // 
            // 
            // toolStripContainer1.ContentPanel
            // 
            this.toolStripContainer1.ContentPanel.Controls.Add(this.PanelPestanas);
            resources.ApplyResources(this.toolStripContainer1.ContentPanel, "toolStripContainer1.ContentPanel");
            resources.ApplyResources(this.toolStripContainer1, "toolStripContainer1");
            this.toolStripContainer1.Name = "toolStripContainer1";
            // 
            // toolStripContainer1.TopToolStripPanel
            // 
            this.toolStripContainer1.TopToolStripPanel.Controls.Add(this.BarraHerramientas);
            // 
            // PanelPestanas
            // 
            this.PanelPestanas.Controls.Add(this.tabAlumnos);
            this.PanelPestanas.Controls.Add(this.tabAsignaturas);
            this.PanelPestanas.Controls.Add(this.tabDepartamentos);
            this.PanelPestanas.Controls.Add(this.tabProfesores);
            resources.ApplyResources(this.PanelPestanas, "PanelPestanas");
            this.PanelPestanas.ImageList = this.imageListPestanas;
            this.PanelPestanas.Name = "PanelPestanas";
            this.PanelPestanas.SelectedIndex = 0;
            // 
            // tabAlumnos
            // 
            resources.ApplyResources(this.tabAlumnos, "tabAlumnos");
            this.tabAlumnos.Controls.Add(this.DistribucionTablaAlumnos);
            this.tabAlumnos.Name = "tabAlumnos";
            this.tabAlumnos.UseVisualStyleBackColor = true;
            // 
            // DistribucionTablaAlumnos
            // 
            resources.ApplyResources(this.DistribucionTablaAlumnos, "DistribucionTablaAlumnos");
            this.DistribucionTablaAlumnos.Controls.Add(this.PanelEdicionAlumno, 1, 0);
            this.DistribucionTablaAlumnos.Controls.Add(this.alumnosDataGridView, 0, 0);
            this.DistribucionTablaAlumnos.Name = "DistribucionTablaAlumnos";
            // 
            // PanelEdicionAlumno
            // 
            this.PanelEdicionAlumno.Controls.Add(this.btnUltimoAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.btnPrimerAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.BtnBorrarAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.BtnAnadirAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.labelNumeroAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.btnSiguienteAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.btnAnteriorAlumno);
            this.PanelEdicionAlumno.Controls.Add(this.labelTituloAlumnos);
            this.PanelEdicionAlumno.Controls.Add(dNILabel);
            this.PanelEdicionAlumno.Controls.Add(this.dNITextBox);
            this.PanelEdicionAlumno.Controls.Add(nombreLabel);
            this.PanelEdicionAlumno.Controls.Add(this.nombreTextBox);
            this.PanelEdicionAlumno.Controls.Add(direccionLabel);
            this.PanelEdicionAlumno.Controls.Add(this.direccionTextBox);
            this.PanelEdicionAlumno.Controls.Add(telefonoLabel);
            this.PanelEdicionAlumno.Controls.Add(this.telefonoTextBox);
            this.PanelEdicionAlumno.Controls.Add(fechaNacimLabel);
            this.PanelEdicionAlumno.Controls.Add(this.fechaNacimDateTimePicker);
            this.PanelEdicionAlumno.Controls.Add(id_AsignaturaLabel);
            this.PanelEdicionAlumno.Controls.Add(this.id_AsignaturaTextBox);
            resources.ApplyResources(this.PanelEdicionAlumno, "PanelEdicionAlumno");
            this.PanelEdicionAlumno.Name = "PanelEdicionAlumno";
            // 
            // btnUltimoAlumno
            // 
            resources.ApplyResources(this.btnUltimoAlumno, "btnUltimoAlumno");
            this.btnUltimoAlumno.Name = "btnUltimoAlumno";
            this.btnUltimoAlumno.UseVisualStyleBackColor = true;
            this.btnUltimoAlumno.Click += new System.EventHandler(this.BtnIrUltimoAlumno_Click);
            // 
            // btnPrimerAlumno
            // 
            resources.ApplyResources(this.btnPrimerAlumno, "btnPrimerAlumno");
            this.btnPrimerAlumno.Name = "btnPrimerAlumno";
            this.btnPrimerAlumno.UseVisualStyleBackColor = true;
            this.btnPrimerAlumno.Click += new System.EventHandler(this.BtnIrPrimerAlumno_Click);
            // 
            // BtnBorrarAlumno
            // 
            resources.ApplyResources(this.BtnBorrarAlumno, "BtnBorrarAlumno");
            this.BtnBorrarAlumno.Image = global::AccessEducacion.Properties.Resources.Subtract_16x;
            this.BtnBorrarAlumno.Name = "BtnBorrarAlumno";
            this.BtnBorrarAlumno.UseVisualStyleBackColor = true;
            this.BtnBorrarAlumno.Click += new System.EventHandler(this.BtnBorrarAlumno_Click);
            // 
            // BtnAnadirAlumno
            // 
            resources.ApplyResources(this.BtnAnadirAlumno, "BtnAnadirAlumno");
            this.BtnAnadirAlumno.Image = global::AccessEducacion.Properties.Resources.Add_thin_10x_16x;
            this.BtnAnadirAlumno.Name = "BtnAnadirAlumno";
            this.BtnAnadirAlumno.UseVisualStyleBackColor = true;
            this.BtnAnadirAlumno.Click += new System.EventHandler(this.BtnAnadirAlumno_Click);
            // 
            // labelNumeroAlumno
            // 
            resources.ApplyResources(this.labelNumeroAlumno, "labelNumeroAlumno");
            this.labelNumeroAlumno.Name = "labelNumeroAlumno";
            // 
            // btnSiguienteAlumno
            // 
            resources.ApplyResources(this.btnSiguienteAlumno, "btnSiguienteAlumno");
            this.btnSiguienteAlumno.Name = "btnSiguienteAlumno";
            this.btnSiguienteAlumno.UseVisualStyleBackColor = true;
            this.btnSiguienteAlumno.Click += new System.EventHandler(this.BtnAlumnoSiguiente_Click);
            // 
            // btnAnteriorAlumno
            // 
            resources.ApplyResources(this.btnAnteriorAlumno, "btnAnteriorAlumno");
            this.btnAnteriorAlumno.Name = "btnAnteriorAlumno";
            this.btnAnteriorAlumno.UseVisualStyleBackColor = true;
            this.btnAnteriorAlumno.Click += new System.EventHandler(this.BtnAlumnoAnterior_Click);
            // 
            // labelTituloAlumnos
            // 
            resources.ApplyResources(this.labelTituloAlumnos, "labelTituloAlumnos");
            this.labelTituloAlumnos.Name = "labelTituloAlumnos";
            // 
            // dNITextBox
            // 
            resources.ApplyResources(this.dNITextBox, "dNITextBox");
            this.dNITextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.alumnosBindingSource, "DNI", true));
            this.dNITextBox.Name = "dNITextBox";
            // 
            // alumnosBindingSource
            // 
            this.alumnosBindingSource.DataMember = "Alumnos";
            this.alumnosBindingSource.DataSource = this.educacionDataSet;
            // 
            // educacionDataSet
            // 
            this.educacionDataSet.DataSetName = "educacionDataSet";
            this.educacionDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // nombreLabel
            // 
            resources.ApplyResources(nombreLabel, "nombreLabel");
            nombreLabel.Name = "nombreLabel";
            // 
            // nombreTextBox
            // 
            resources.ApplyResources(this.nombreTextBox, "nombreTextBox");
            this.nombreTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.alumnosBindingSource, "Nombre", true));
            this.nombreTextBox.Name = "nombreTextBox";
            // 
            // direccionLabel
            // 
            resources.ApplyResources(direccionLabel, "direccionLabel");
            direccionLabel.Name = "direccionLabel";
            // 
            // direccionTextBox
            // 
            resources.ApplyResources(this.direccionTextBox, "direccionTextBox");
            this.direccionTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.alumnosBindingSource, "Direccion", true));
            this.direccionTextBox.Name = "direccionTextBox";
            // 
            // telefonoLabel
            // 
            resources.ApplyResources(telefonoLabel, "telefonoLabel");
            telefonoLabel.Name = "telefonoLabel";
            // 
            // telefonoTextBox
            // 
            resources.ApplyResources(this.telefonoTextBox, "telefonoTextBox");
            this.telefonoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.alumnosBindingSource, "Telefono", true));
            this.telefonoTextBox.Name = "telefonoTextBox";
            // 
            // fechaNacimLabel
            // 
            resources.ApplyResources(fechaNacimLabel, "fechaNacimLabel");
            fechaNacimLabel.Name = "fechaNacimLabel";
            // 
            // fechaNacimDateTimePicker
            // 
            resources.ApplyResources(this.fechaNacimDateTimePicker, "fechaNacimDateTimePicker");
            this.fechaNacimDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.alumnosBindingSource, "FechaNacim", true));
            this.fechaNacimDateTimePicker.Name = "fechaNacimDateTimePicker";
            // 
            // id_AsignaturaLabel
            // 
            resources.ApplyResources(id_AsignaturaLabel, "id_AsignaturaLabel");
            id_AsignaturaLabel.Name = "id_AsignaturaLabel";
            // 
            // id_AsignaturaTextBox
            // 
            resources.ApplyResources(this.id_AsignaturaTextBox, "id_AsignaturaTextBox");
            this.id_AsignaturaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.alumnosBindingSource, "Id_Asignatura", true));
            this.id_AsignaturaTextBox.Name = "id_AsignaturaTextBox";
            // 
            // alumnosDataGridView
            // 
            this.alumnosDataGridView.AllowUserToAddRows = false;
            this.alumnosDataGridView.AllowUserToDeleteRows = false;
            this.alumnosDataGridView.AllowUserToOrderColumns = true;
            this.alumnosDataGridView.AutoGenerateColumns = false;
            this.alumnosDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.alumnosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.alumnosDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            this.alumnosDataGridView.DataSource = this.alumnosBindingSource;
            resources.ApplyResources(this.alumnosDataGridView, "alumnosDataGridView");
            this.alumnosDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.alumnosDataGridView.Name = "alumnosDataGridView";
            this.alumnosDataGridView.ReadOnly = true;
            this.alumnosDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridAlumnos_CellClick);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "DNI";
            resources.ApplyResources(this.dataGridViewTextBoxColumn1, "dataGridViewTextBoxColumn1");
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Nombre";
            resources.ApplyResources(this.dataGridViewTextBoxColumn2, "dataGridViewTextBoxColumn2");
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Direccion";
            resources.ApplyResources(this.dataGridViewTextBoxColumn3, "dataGridViewTextBoxColumn3");
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "Telefono";
            resources.ApplyResources(this.dataGridViewTextBoxColumn4, "dataGridViewTextBoxColumn4");
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "FechaNacim";
            resources.ApplyResources(this.dataGridViewTextBoxColumn5, "dataGridViewTextBoxColumn5");
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Id_Asignatura";
            resources.ApplyResources(this.dataGridViewTextBoxColumn6, "dataGridViewTextBoxColumn6");
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            // 
            // tabAsignaturas
            // 
            this.tabAsignaturas.Controls.Add(this.DistribucionTablaAsignaturas);
            resources.ApplyResources(this.tabAsignaturas, "tabAsignaturas");
            this.tabAsignaturas.Name = "tabAsignaturas";
            this.tabAsignaturas.UseVisualStyleBackColor = true;
            // 
            // DistribucionTablaAsignaturas
            // 
            resources.ApplyResources(this.DistribucionTablaAsignaturas, "DistribucionTablaAsignaturas");
            this.DistribucionTablaAsignaturas.Controls.Add(this.asignaturasDataGridView, 0, 0);
            this.DistribucionTablaAsignaturas.Controls.Add(this.PanelEdicionAsignatura, 1, 0);
            this.DistribucionTablaAsignaturas.Name = "DistribucionTablaAsignaturas";
            // 
            // asignaturasDataGridView
            // 
            this.asignaturasDataGridView.AllowUserToAddRows = false;
            this.asignaturasDataGridView.AllowUserToDeleteRows = false;
            this.asignaturasDataGridView.AutoGenerateColumns = false;
            this.asignaturasDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.asignaturasDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.asignaturasDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9});
            this.asignaturasDataGridView.DataSource = this.asignaturasBindingSource;
            resources.ApplyResources(this.asignaturasDataGridView, "asignaturasDataGridView");
            this.asignaturasDataGridView.Name = "asignaturasDataGridView";
            this.asignaturasDataGridView.ReadOnly = true;
            this.asignaturasDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GirdAsignaturas_CellClick);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "Id";
            resources.ApplyResources(this.dataGridViewTextBoxColumn7, "dataGridViewTextBoxColumn7");
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "Nombre";
            resources.ApplyResources(this.dataGridViewTextBoxColumn8, "dataGridViewTextBoxColumn8");
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "DNI_Profesor";
            resources.ApplyResources(this.dataGridViewTextBoxColumn9, "dataGridViewTextBoxColumn9");
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            // 
            // asignaturasBindingSource
            // 
            this.asignaturasBindingSource.DataMember = "Asignaturas";
            this.asignaturasBindingSource.DataSource = this.educacionDataSet;
            // 
            // PanelEdicionAsignatura
            // 
            resources.ApplyResources(this.PanelEdicionAsignatura, "PanelEdicionAsignatura");
            this.PanelEdicionAsignatura.Controls.Add(idLabel);
            this.PanelEdicionAsignatura.Controls.Add(this.idTextBox);
            this.PanelEdicionAsignatura.Controls.Add(nombreLabel1);
            this.PanelEdicionAsignatura.Controls.Add(this.nombreTextBox1);
            this.PanelEdicionAsignatura.Controls.Add(dNI_ProfesorLabel);
            this.PanelEdicionAsignatura.Controls.Add(this.dNI_ProfesorTextBox);
            this.PanelEdicionAsignatura.Controls.Add(this.btnUltimaAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.btnPrimeraAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.btnEliminarAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.btnAnadirAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.labelNumeroAsignaturas);
            this.PanelEdicionAsignatura.Controls.Add(this.btnSiguienteAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.btnAnteriorAsignatura);
            this.PanelEdicionAsignatura.Controls.Add(this.labelTituloAsignaturas);
            this.PanelEdicionAsignatura.Name = "PanelEdicionAsignatura";
            // 
            // idLabel
            // 
            resources.ApplyResources(idLabel, "idLabel");
            idLabel.Name = "idLabel";
            // 
            // idTextBox
            // 
            resources.ApplyResources(this.idTextBox, "idTextBox");
            this.idTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.asignaturasBindingSource, "Id", true));
            this.idTextBox.Name = "idTextBox";
            // 
            // nombreLabel1
            // 
            resources.ApplyResources(nombreLabel1, "nombreLabel1");
            nombreLabel1.Name = "nombreLabel1";
            // 
            // nombreTextBox1
            // 
            resources.ApplyResources(this.nombreTextBox1, "nombreTextBox1");
            this.nombreTextBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.asignaturasBindingSource, "Nombre", true));
            this.nombreTextBox1.Name = "nombreTextBox1";
            // 
            // dNI_ProfesorLabel
            // 
            resources.ApplyResources(dNI_ProfesorLabel, "dNI_ProfesorLabel");
            dNI_ProfesorLabel.Name = "dNI_ProfesorLabel";
            // 
            // dNI_ProfesorTextBox
            // 
            resources.ApplyResources(this.dNI_ProfesorTextBox, "dNI_ProfesorTextBox");
            this.dNI_ProfesorTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.asignaturasBindingSource, "DNI_Profesor", true));
            this.dNI_ProfesorTextBox.Name = "dNI_ProfesorTextBox";
            // 
            // btnUltimaAsignatura
            // 
            resources.ApplyResources(this.btnUltimaAsignatura, "btnUltimaAsignatura");
            this.btnUltimaAsignatura.Name = "btnUltimaAsignatura";
            this.btnUltimaAsignatura.UseVisualStyleBackColor = true;
            this.btnUltimaAsignatura.Click += new System.EventHandler(this.BtnIrUltimaAsignatura_Click);
            // 
            // btnPrimeraAsignatura
            // 
            resources.ApplyResources(this.btnPrimeraAsignatura, "btnPrimeraAsignatura");
            this.btnPrimeraAsignatura.Name = "btnPrimeraAsignatura";
            this.btnPrimeraAsignatura.UseVisualStyleBackColor = true;
            this.btnPrimeraAsignatura.Click += new System.EventHandler(this.BtnIrPrimeraAsignatura_Click);
            // 
            // btnEliminarAsignatura
            // 
            resources.ApplyResources(this.btnEliminarAsignatura, "btnEliminarAsignatura");
            this.btnEliminarAsignatura.Image = global::AccessEducacion.Properties.Resources.Subtract_16x;
            this.btnEliminarAsignatura.Name = "btnEliminarAsignatura";
            this.btnEliminarAsignatura.UseVisualStyleBackColor = true;
            this.btnEliminarAsignatura.Click += new System.EventHandler(this.BtnEliminarAsignatura_Click);
            // 
            // btnAnadirAsignatura
            // 
            resources.ApplyResources(this.btnAnadirAsignatura, "btnAnadirAsignatura");
            this.btnAnadirAsignatura.Image = global::AccessEducacion.Properties.Resources.Add_thin_10x_16x;
            this.btnAnadirAsignatura.Name = "btnAnadirAsignatura";
            this.btnAnadirAsignatura.UseVisualStyleBackColor = true;
            this.btnAnadirAsignatura.Click += new System.EventHandler(this.BtnAnadirAsignatura_Click);
            // 
            // labelNumeroAsignaturas
            // 
            resources.ApplyResources(this.labelNumeroAsignaturas, "labelNumeroAsignaturas");
            this.labelNumeroAsignaturas.Name = "labelNumeroAsignaturas";
            // 
            // btnSiguienteAsignatura
            // 
            resources.ApplyResources(this.btnSiguienteAsignatura, "btnSiguienteAsignatura");
            this.btnSiguienteAsignatura.Name = "btnSiguienteAsignatura";
            this.btnSiguienteAsignatura.UseVisualStyleBackColor = true;
            this.btnSiguienteAsignatura.Click += new System.EventHandler(this.BtnAsignaturaSiguiente_Click);
            // 
            // btnAnteriorAsignatura
            // 
            resources.ApplyResources(this.btnAnteriorAsignatura, "btnAnteriorAsignatura");
            this.btnAnteriorAsignatura.Name = "btnAnteriorAsignatura";
            this.btnAnteriorAsignatura.UseVisualStyleBackColor = true;
            this.btnAnteriorAsignatura.Click += new System.EventHandler(this.BtnAsignaturaAnterior_Click);
            // 
            // labelTituloAsignaturas
            // 
            resources.ApplyResources(this.labelTituloAsignaturas, "labelTituloAsignaturas");
            this.labelTituloAsignaturas.Name = "labelTituloAsignaturas";
            // 
            // tabDepartamentos
            // 
            this.tabDepartamentos.Controls.Add(this.DistribucionTablaDepartamentos);
            resources.ApplyResources(this.tabDepartamentos, "tabDepartamentos");
            this.tabDepartamentos.Name = "tabDepartamentos";
            this.tabDepartamentos.UseVisualStyleBackColor = true;
            // 
            // DistribucionTablaDepartamentos
            // 
            resources.ApplyResources(this.DistribucionTablaDepartamentos, "DistribucionTablaDepartamentos");
            this.DistribucionTablaDepartamentos.Controls.Add(this.PanelEdicionDepartamentos, 1, 0);
            this.DistribucionTablaDepartamentos.Controls.Add(this.departamentosDataGridView, 0, 0);
            this.DistribucionTablaDepartamentos.Name = "DistribucionTablaDepartamentos";
            // 
            // PanelEdicionDepartamentos
            // 
            resources.ApplyResources(this.PanelEdicionDepartamentos, "PanelEdicionDepartamentos");
            this.PanelEdicionDepartamentos.Controls.Add(idLabel1);
            this.PanelEdicionDepartamentos.Controls.Add(this.idTextBoxDept);
            this.PanelEdicionDepartamentos.Controls.Add(nombreLabel2);
            this.PanelEdicionDepartamentos.Controls.Add(this.nombreTextBox2);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnUltimoDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnPrimerDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnEliminarDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnAnadirDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.labelNumeroDepartamentos);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnSiguienteDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.btnAnteriorDepartamento);
            this.PanelEdicionDepartamentos.Controls.Add(this.labelTituloDepartamentos);
            this.PanelEdicionDepartamentos.Name = "PanelEdicionDepartamentos";
            // 
            // idLabel1
            // 
            resources.ApplyResources(idLabel1, "idLabel1");
            idLabel1.Name = "idLabel1";
            // 
            // idTextBoxDept
            // 
            resources.ApplyResources(this.idTextBoxDept, "idTextBoxDept");
            this.idTextBoxDept.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.departamentosBindingSource, "Id", true));
            this.idTextBoxDept.Name = "idTextBoxDept";
            // 
            // departamentosBindingSource
            // 
            this.departamentosBindingSource.DataMember = "Departamentos";
            this.departamentosBindingSource.DataSource = this.educacionDataSet;
            // 
            // nombreLabel2
            // 
            resources.ApplyResources(nombreLabel2, "nombreLabel2");
            nombreLabel2.Name = "nombreLabel2";
            // 
            // nombreTextBox2
            // 
            resources.ApplyResources(this.nombreTextBox2, "nombreTextBox2");
            this.nombreTextBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.departamentosBindingSource, "Nombre", true));
            this.nombreTextBox2.Name = "nombreTextBox2";
            // 
            // btnUltimoDepartamento
            // 
            resources.ApplyResources(this.btnUltimoDepartamento, "btnUltimoDepartamento");
            this.btnUltimoDepartamento.Name = "btnUltimoDepartamento";
            this.btnUltimoDepartamento.UseVisualStyleBackColor = true;
            this.btnUltimoDepartamento.Click += new System.EventHandler(this.btnUltimoDepartamento_Click);
            // 
            // btnPrimerDepartamento
            // 
            resources.ApplyResources(this.btnPrimerDepartamento, "btnPrimerDepartamento");
            this.btnPrimerDepartamento.Name = "btnPrimerDepartamento";
            this.btnPrimerDepartamento.UseVisualStyleBackColor = true;
            this.btnPrimerDepartamento.Click += new System.EventHandler(this.BtnIrPrimerDepartamento_Click);
            // 
            // btnEliminarDepartamento
            // 
            resources.ApplyResources(this.btnEliminarDepartamento, "btnEliminarDepartamento");
            this.btnEliminarDepartamento.Image = global::AccessEducacion.Properties.Resources.Subtract_16x;
            this.btnEliminarDepartamento.Name = "btnEliminarDepartamento";
            this.btnEliminarDepartamento.UseVisualStyleBackColor = true;
            this.btnEliminarDepartamento.Click += new System.EventHandler(this.btnEliminarDepartamento_Click);
            // 
            // btnAnadirDepartamento
            // 
            resources.ApplyResources(this.btnAnadirDepartamento, "btnAnadirDepartamento");
            this.btnAnadirDepartamento.Image = global::AccessEducacion.Properties.Resources.Add_thin_10x_16x;
            this.btnAnadirDepartamento.Name = "btnAnadirDepartamento";
            this.btnAnadirDepartamento.UseVisualStyleBackColor = true;
            this.btnAnadirDepartamento.Click += new System.EventHandler(this.btnAnadirDepartamento_Click);
            // 
            // labelNumeroDepartamentos
            // 
            resources.ApplyResources(this.labelNumeroDepartamentos, "labelNumeroDepartamentos");
            this.labelNumeroDepartamentos.Name = "labelNumeroDepartamentos";
            // 
            // btnSiguienteDepartamento
            // 
            resources.ApplyResources(this.btnSiguienteDepartamento, "btnSiguienteDepartamento");
            this.btnSiguienteDepartamento.Name = "btnSiguienteDepartamento";
            this.btnSiguienteDepartamento.UseVisualStyleBackColor = true;
            this.btnSiguienteDepartamento.Click += new System.EventHandler(this.btnSiguienteDepartamento_Click);
            // 
            // btnAnteriorDepartamento
            // 
            resources.ApplyResources(this.btnAnteriorDepartamento, "btnAnteriorDepartamento");
            this.btnAnteriorDepartamento.Name = "btnAnteriorDepartamento";
            this.btnAnteriorDepartamento.UseVisualStyleBackColor = true;
            this.btnAnteriorDepartamento.Click += new System.EventHandler(this.BtnAnteriorDepartamento_Click);
            // 
            // labelTituloDepartamentos
            // 
            resources.ApplyResources(this.labelTituloDepartamentos, "labelTituloDepartamentos");
            this.labelTituloDepartamentos.Name = "labelTituloDepartamentos";
            // 
            // departamentosDataGridView
            // 
            this.departamentosDataGridView.AllowUserToAddRows = false;
            this.departamentosDataGridView.AllowUserToDeleteRows = false;
            this.departamentosDataGridView.AllowUserToOrderColumns = true;
            this.departamentosDataGridView.AutoGenerateColumns = false;
            this.departamentosDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.departamentosDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.departamentosDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.dataGridViewTextBoxColumn11});
            this.departamentosDataGridView.DataSource = this.departamentosBindingSource;
            resources.ApplyResources(this.departamentosDataGridView, "departamentosDataGridView");
            this.departamentosDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.departamentosDataGridView.Name = "departamentosDataGridView";
            this.departamentosDataGridView.ReadOnly = true;
            this.departamentosDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.departamentosDataGridView_CellClick);
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Id";
            resources.ApplyResources(this.Column1, "Column1");
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "Nombre";
            resources.ApplyResources(this.dataGridViewTextBoxColumn11, "dataGridViewTextBoxColumn11");
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            // 
            // tabProfesores
            // 
            this.tabProfesores.Controls.Add(this.DistribucionTablaProfesores);
            resources.ApplyResources(this.tabProfesores, "tabProfesores");
            this.tabProfesores.Name = "tabProfesores";
            this.tabProfesores.UseVisualStyleBackColor = true;
            // 
            // DistribucionTablaProfesores
            // 
            resources.ApplyResources(this.DistribucionTablaProfesores, "DistribucionTablaProfesores");
            this.DistribucionTablaProfesores.Controls.Add(this.PanelEdicionProfesores, 1, 0);
            this.DistribucionTablaProfesores.Controls.Add(this.profesoresDataGridView, 0, 0);
            this.DistribucionTablaProfesores.Name = "DistribucionTablaProfesores";
            // 
            // PanelEdicionProfesores
            // 
            resources.ApplyResources(this.PanelEdicionProfesores, "PanelEdicionProfesores");
            this.PanelEdicionProfesores.Controls.Add(dNILabel1);
            this.PanelEdicionProfesores.Controls.Add(this.dNITextBoxProf);
            this.PanelEdicionProfesores.Controls.Add(nombreLabel3);
            this.PanelEdicionProfesores.Controls.Add(this.nombreTextBox3);
            this.PanelEdicionProfesores.Controls.Add(id_DepartamentoLabel);
            this.PanelEdicionProfesores.Controls.Add(this.id_DepartamentoTextBox);
            this.PanelEdicionProfesores.Controls.Add(this.BtnUltimoProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.BtnPrimerProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.BtnEliminarProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.BtnAnadirProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.labelNumeroProfesores);
            this.PanelEdicionProfesores.Controls.Add(this.BtnSiguienteProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.BtnAnteriorProfesor);
            this.PanelEdicionProfesores.Controls.Add(this.labelTituloProfesores);
            this.PanelEdicionProfesores.Name = "PanelEdicionProfesores";
            // 
            // dNILabel1
            // 
            resources.ApplyResources(dNILabel1, "dNILabel1");
            dNILabel1.Name = "dNILabel1";
            // 
            // dNITextBoxProf
            // 
            resources.ApplyResources(this.dNITextBoxProf, "dNITextBoxProf");
            this.dNITextBoxProf.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.profesoresBindingSource, "DNI", true));
            this.dNITextBoxProf.Name = "dNITextBoxProf";
            // 
            // profesoresBindingSource
            // 
            this.profesoresBindingSource.DataMember = "Profesores";
            this.profesoresBindingSource.DataSource = this.educacionDataSet;
            // 
            // nombreLabel3
            // 
            resources.ApplyResources(nombreLabel3, "nombreLabel3");
            nombreLabel3.Name = "nombreLabel3";
            // 
            // nombreTextBox3
            // 
            resources.ApplyResources(this.nombreTextBox3, "nombreTextBox3");
            this.nombreTextBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.profesoresBindingSource, "Nombre", true));
            this.nombreTextBox3.Name = "nombreTextBox3";
            // 
            // id_DepartamentoLabel
            // 
            resources.ApplyResources(id_DepartamentoLabel, "id_DepartamentoLabel");
            id_DepartamentoLabel.Name = "id_DepartamentoLabel";
            // 
            // id_DepartamentoTextBox
            // 
            resources.ApplyResources(this.id_DepartamentoTextBox, "id_DepartamentoTextBox");
            this.id_DepartamentoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.profesoresBindingSource, "Id_Departamento", true));
            this.id_DepartamentoTextBox.Name = "id_DepartamentoTextBox";
            // 
            // BtnUltimoProfesor
            // 
            resources.ApplyResources(this.BtnUltimoProfesor, "BtnUltimoProfesor");
            this.BtnUltimoProfesor.Name = "BtnUltimoProfesor";
            this.BtnUltimoProfesor.UseVisualStyleBackColor = true;
            this.BtnUltimoProfesor.Click += new System.EventHandler(this.BtnUltimoProfesor_Click);
            // 
            // BtnPrimerProfesor
            // 
            resources.ApplyResources(this.BtnPrimerProfesor, "BtnPrimerProfesor");
            this.BtnPrimerProfesor.Name = "BtnPrimerProfesor";
            this.BtnPrimerProfesor.UseVisualStyleBackColor = true;
            this.BtnPrimerProfesor.Click += new System.EventHandler(this.BtnPrimerProfesor_Click);
            // 
            // BtnEliminarProfesor
            // 
            resources.ApplyResources(this.BtnEliminarProfesor, "BtnEliminarProfesor");
            this.BtnEliminarProfesor.Image = global::AccessEducacion.Properties.Resources.Subtract_16x;
            this.BtnEliminarProfesor.Name = "BtnEliminarProfesor";
            this.BtnEliminarProfesor.UseVisualStyleBackColor = true;
            this.BtnEliminarProfesor.Click += new System.EventHandler(this.BtnEliminarProfesor_Click);
            // 
            // BtnAnadirProfesor
            // 
            resources.ApplyResources(this.BtnAnadirProfesor, "BtnAnadirProfesor");
            this.BtnAnadirProfesor.Image = global::AccessEducacion.Properties.Resources.Add_thin_10x_16x;
            this.BtnAnadirProfesor.Name = "BtnAnadirProfesor";
            this.BtnAnadirProfesor.UseVisualStyleBackColor = true;
            this.BtnAnadirProfesor.Click += new System.EventHandler(this.BtnAnadirProfesor_Click);
            // 
            // labelNumeroProfesores
            // 
            resources.ApplyResources(this.labelNumeroProfesores, "labelNumeroProfesores");
            this.labelNumeroProfesores.Name = "labelNumeroProfesores";
            // 
            // BtnSiguienteProfesor
            // 
            resources.ApplyResources(this.BtnSiguienteProfesor, "BtnSiguienteProfesor");
            this.BtnSiguienteProfesor.Name = "BtnSiguienteProfesor";
            this.BtnSiguienteProfesor.UseVisualStyleBackColor = true;
            this.BtnSiguienteProfesor.Click += new System.EventHandler(this.BtnSiguienteProfesor_Click);
            // 
            // BtnAnteriorProfesor
            // 
            resources.ApplyResources(this.BtnAnteriorProfesor, "BtnAnteriorProfesor");
            this.BtnAnteriorProfesor.Name = "BtnAnteriorProfesor";
            this.BtnAnteriorProfesor.UseVisualStyleBackColor = true;
            this.BtnAnteriorProfesor.Click += new System.EventHandler(this.BtnAnteriorProfesor_Click);
            // 
            // labelTituloProfesores
            // 
            resources.ApplyResources(this.labelTituloProfesores, "labelTituloProfesores");
            this.labelTituloProfesores.Name = "labelTituloProfesores";
            // 
            // profesoresDataGridView
            // 
            this.profesoresDataGridView.AllowUserToAddRows = false;
            this.profesoresDataGridView.AllowUserToDeleteRows = false;
            this.profesoresDataGridView.AllowUserToOrderColumns = true;
            this.profesoresDataGridView.AutoGenerateColumns = false;
            this.profesoresDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.profesoresDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.profesoresDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn12,
            this.Id_Departamento});
            this.profesoresDataGridView.DataSource = this.profesoresBindingSource;
            resources.ApplyResources(this.profesoresDataGridView, "profesoresDataGridView");
            this.profesoresDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.profesoresDataGridView.Name = "profesoresDataGridView";
            this.profesoresDataGridView.ReadOnly = true;
            this.profesoresDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.profesoresDataGridView_CellClick);
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DNI";
            resources.ApplyResources(this.dataGridViewTextBoxColumn10, "dataGridViewTextBoxColumn10");
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "Nombre";
            resources.ApplyResources(this.dataGridViewTextBoxColumn12, "dataGridViewTextBoxColumn12");
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            // 
            // Id_Departamento
            // 
            this.Id_Departamento.DataPropertyName = "Id_Departamento";
            resources.ApplyResources(this.Id_Departamento, "Id_Departamento");
            this.Id_Departamento.Name = "Id_Departamento";
            this.Id_Departamento.ReadOnly = true;
            // 
            // imageListPestanas
            // 
            this.imageListPestanas.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListPestanas.ImageStream")));
            this.imageListPestanas.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListPestanas.Images.SetKeyName(0, "User");
            this.imageListPestanas.Images.SetKeyName(1, "Book");
            this.imageListPestanas.Images.SetKeyName(2, "Department");
            this.imageListPestanas.Images.SetKeyName(3, "Teacher");
            // 
            // BarraHerramientas
            // 
            resources.ApplyResources(this.BarraHerramientas, "BarraHerramientas");
            this.BarraHerramientas.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.BarraHerramientas.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnGuardar});
            this.BarraHerramientas.Name = "BarraHerramientas";
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.BtnGuardar.Image = global::AccessEducacion.Properties.Resources.Save_16x;
            resources.ApplyResources(this.BtnGuardar, "BtnGuardar");
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BarraMenu
            // 
            this.BarraMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuArchivo,
            this.MenuConsultas,
            this.MenuVista});
            resources.ApplyResources(this.BarraMenu, "BarraMenu");
            this.BarraMenu.Name = "BarraMenu";
            // 
            // MenuArchivo
            // 
            this.MenuArchivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemGuardar,
            this.salirToolStripMenuItem});
            this.MenuArchivo.Name = "MenuArchivo";
            resources.ApplyResources(this.MenuArchivo, "MenuArchivo");
            // 
            // MenuItemGuardar
            // 
            this.MenuItemGuardar.Image = global::AccessEducacion.Properties.Resources.Save_16x;
            this.MenuItemGuardar.Name = "MenuItemGuardar";
            resources.ApplyResources(this.MenuItemGuardar, "MenuItemGuardar");
            this.MenuItemGuardar.Click += new System.EventHandler(this.MenuItemGuardar_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Image = global::AccessEducacion.Properties.Resources.CloseSolution_16x;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            resources.ApplyResources(this.salirToolStripMenuItem, "salirToolStripMenuItem");
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.SalirMenuItem_Click);
            // 
            // MenuConsultas
            // 
            this.MenuConsultas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemAlumnosMayores21,
            this.MenuItemProfesoresLengua,
            this.toolStripSeparator2,
            this.MenuItemConsultaEspecifica});
            this.MenuConsultas.Name = "MenuConsultas";
            resources.ApplyResources(this.MenuConsultas, "MenuConsultas");
            // 
            // MenuItemAlumnosMayores21
            // 
            this.MenuItemAlumnosMayores21.Image = global::AccessEducacion.Properties.Resources.OpenQueryView_16x;
            this.MenuItemAlumnosMayores21.Name = "MenuItemAlumnosMayores21";
            resources.ApplyResources(this.MenuItemAlumnosMayores21, "MenuItemAlumnosMayores21");
            this.MenuItemAlumnosMayores21.Click += new System.EventHandler(this.MenuItemAlumnosMayores21_Click);
            // 
            // MenuItemProfesoresLengua
            // 
            this.MenuItemProfesoresLengua.Image = global::AccessEducacion.Properties.Resources.OpenQueryView_16x;
            this.MenuItemProfesoresLengua.Name = "MenuItemProfesoresLengua";
            resources.ApplyResources(this.MenuItemProfesoresLengua, "MenuItemProfesoresLengua");
            this.MenuItemProfesoresLengua.Click += new System.EventHandler(this.ConsultaProfesoresDeptLengua_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // MenuItemConsultaEspecifica
            // 
            this.MenuItemConsultaEspecifica.Name = "MenuItemConsultaEspecifica";
            resources.ApplyResources(this.MenuItemConsultaEspecifica, "MenuItemConsultaEspecifica");
            this.MenuItemConsultaEspecifica.Click += new System.EventHandler(this.MenuItemConsultaEspecifica_Click);
            // 
            // MenuVista
            // 
            this.MenuVista.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemVistaTabla,
            this.MenuItemVistaEdicion,
            this.toolStripSeparator1,
            this.MenuItemVistaAmbos});
            this.MenuVista.Name = "MenuVista";
            resources.ApplyResources(this.MenuVista, "MenuVista");
            // 
            // MenuItemVistaTabla
            // 
            this.MenuItemVistaTabla.Image = global::AccessEducacion.Properties.Resources.eye_16xLG;
            this.MenuItemVistaTabla.Name = "MenuItemVistaTabla";
            resources.ApplyResources(this.MenuItemVistaTabla, "MenuItemVistaTabla");
            this.MenuItemVistaTabla.Click += new System.EventHandler(this.MenuItemVistaTabla_Click);
            // 
            // MenuItemVistaEdicion
            // 
            this.MenuItemVistaEdicion.Image = global::AccessEducacion.Properties.Resources.Pen4_16x;
            this.MenuItemVistaEdicion.Name = "MenuItemVistaEdicion";
            resources.ApplyResources(this.MenuItemVistaEdicion, "MenuItemVistaEdicion");
            this.MenuItemVistaEdicion.Click += new System.EventHandler(this.MenuItemVistaEdicion_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // MenuItemVistaAmbos
            // 
            this.MenuItemVistaAmbos.Checked = true;
            this.MenuItemVistaAmbos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.MenuItemVistaAmbos.Name = "MenuItemVistaAmbos";
            resources.ApplyResources(this.MenuItemVistaAmbos, "MenuItemVistaAmbos");
            this.MenuItemVistaAmbos.Click += new System.EventHandler(this.MenuItemVistaAmbos_Click);
            // 
            // alumnosMayores21TableAdapter
            // 
            this.alumnosMayores21TableAdapter.ClearBeforeFill = true;
            // 
            // alumnosTableAdapter
            // 
            this.alumnosTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AlumnosTableAdapter = this.alumnosTableAdapter;
            this.tableAdapterManager.AsignaturasTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.DepartamentosTableAdapter = null;
            this.tableAdapterManager.ProfesoresTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = AccessEducacion.educacionDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // asignaturasTableAdapter
            // 
            this.asignaturasTableAdapter.ClearBeforeFill = true;
            // 
            // alumnosMayores21BindingSource
            // 
            this.alumnosMayores21BindingSource.DataMember = "AlumnosMayores21";
            this.alumnosMayores21BindingSource.DataSource = this.educacionDataSet;
            // 
            // departamentosTableAdapter
            // 
            this.departamentosTableAdapter.ClearBeforeFill = true;
            // 
            // profesoresTableAdapter
            // 
            this.profesoresTableAdapter.ClearBeforeFill = true;
            // 
            // profesoresDeptLenguaBindingSource
            // 
            this.profesoresDeptLenguaBindingSource.DataMember = "ProfesoresDeptLengua";
            this.profesoresDeptLenguaBindingSource.DataSource = this.educacionDataSet;
            // 
            // profesoresDeptLenguaTableAdapter
            // 
            this.profesoresDeptLenguaTableAdapter.ClearBeforeFill = true;
            // 
            // VentanaPrincipal
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.toolStripContainer1);
            this.Controls.Add(this.BarraMenu);
            this.MainMenuStrip = this.BarraMenu;
            this.Name = "VentanaPrincipal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VentanaPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.VentanaPrincipal_Load);
            this.toolStripContainer1.ContentPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.toolStripContainer1.TopToolStripPanel.PerformLayout();
            this.toolStripContainer1.ResumeLayout(false);
            this.toolStripContainer1.PerformLayout();
            this.PanelPestanas.ResumeLayout(false);
            this.tabAlumnos.ResumeLayout(false);
            this.DistribucionTablaAlumnos.ResumeLayout(false);
            this.PanelEdicionAlumno.ResumeLayout(false);
            this.PanelEdicionAlumno.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.educacionDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosDataGridView)).EndInit();
            this.tabAsignaturas.ResumeLayout(false);
            this.DistribucionTablaAsignaturas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.asignaturasBindingSource)).EndInit();
            this.PanelEdicionAsignatura.ResumeLayout(false);
            this.PanelEdicionAsignatura.PerformLayout();
            this.tabDepartamentos.ResumeLayout(false);
            this.DistribucionTablaDepartamentos.ResumeLayout(false);
            this.PanelEdicionDepartamentos.ResumeLayout(false);
            this.PanelEdicionDepartamentos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.departamentosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.departamentosDataGridView)).EndInit();
            this.tabProfesores.ResumeLayout(false);
            this.DistribucionTablaProfesores.ResumeLayout(false);
            this.PanelEdicionProfesores.ResumeLayout(false);
            this.PanelEdicionProfesores.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresDataGridView)).EndInit();
            this.BarraHerramientas.ResumeLayout(false);
            this.BarraHerramientas.PerformLayout();
            this.BarraMenu.ResumeLayout(false);
            this.BarraMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.alumnosMayores21BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.profesoresDeptLenguaBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl PanelPestanas;
        private System.Windows.Forms.TabPage tabAlumnos;
        private System.Windows.Forms.TabPage tabAsignaturas;
        private System.Windows.Forms.TabPage tabDepartamentos;
        private System.Windows.Forms.TabPage tabProfesores;
        private System.Windows.Forms.ImageList imageListPestanas;
        private educacionDataSet educacionDataSet;
        private System.Windows.Forms.BindingSource alumnosBindingSource;
        private educacionDataSetTableAdapters.AlumnosTableAdapter alumnosTableAdapter;
        private educacionDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView alumnosDataGridView;
        private System.Windows.Forms.ToolStripContainer toolStripContainer1;
        private System.Windows.Forms.ToolStrip BarraHerramientas;
        private System.Windows.Forms.ToolStripButton BtnGuardar;
        private System.Windows.Forms.Panel PanelEdicionAlumno;
        private System.Windows.Forms.Button btnSiguienteAlumno;
        private System.Windows.Forms.Button btnAnteriorAlumno;
        private System.Windows.Forms.Label labelTituloAlumnos;
        private System.Windows.Forms.TextBox dNITextBox;
        private System.Windows.Forms.TextBox nombreTextBox;
        private System.Windows.Forms.TextBox direccionTextBox;
        private System.Windows.Forms.TextBox telefonoTextBox;
        private System.Windows.Forms.DateTimePicker fechaNacimDateTimePicker;
        private System.Windows.Forms.TextBox id_AsignaturaTextBox;
        private System.Windows.Forms.TableLayoutPanel DistribucionTablaAlumnos;
        private educacionDataSetTableAdapters.AlumnosMayores21TableAdapter alumnosMayores21TableAdapter;
        private System.Windows.Forms.MenuStrip BarraMenu;
        private System.Windows.Forms.ToolStripMenuItem MenuArchivo;
        private System.Windows.Forms.ToolStripMenuItem MenuItemGuardar;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Label labelNumeroAlumno;
        private System.Windows.Forms.Button BtnAnadirAlumno;
        private System.Windows.Forms.Button BtnBorrarAlumno;
        private System.Windows.Forms.ToolStripMenuItem MenuConsultas;
        private System.Windows.Forms.ToolStripMenuItem MenuItemAlumnosMayores21;
        private System.Windows.Forms.ToolStripMenuItem MenuItemProfesoresLengua;
        private System.Windows.Forms.Button btnPrimerAlumno;
        private System.Windows.Forms.Button btnUltimoAlumno;
        private System.Windows.Forms.TableLayoutPanel DistribucionTablaAsignaturas;
        private System.Windows.Forms.Panel PanelEdicionAsignatura;
        private System.Windows.Forms.Button btnUltimaAsignatura;
        private System.Windows.Forms.Button btnPrimeraAsignatura;
        private System.Windows.Forms.Button btnEliminarAsignatura;
        private System.Windows.Forms.Button btnAnadirAsignatura;
        private System.Windows.Forms.Label labelNumeroAsignaturas;
        private System.Windows.Forms.Button btnSiguienteAsignatura;
        private System.Windows.Forms.Button btnAnteriorAsignatura;
        private System.Windows.Forms.Label labelTituloAsignaturas;
        private System.Windows.Forms.BindingSource asignaturasBindingSource;
        private educacionDataSetTableAdapters.AsignaturasTableAdapter asignaturasTableAdapter;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.TextBox nombreTextBox1;
        private System.Windows.Forms.TextBox dNI_ProfesorTextBox;
        private System.Windows.Forms.DataGridView asignaturasDataGridView;
        private System.Windows.Forms.ToolStripMenuItem MenuVista;
        private System.Windows.Forms.ToolStripMenuItem MenuItemVistaTabla;
        private System.Windows.Forms.ToolStripMenuItem MenuItemVistaEdicion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem MenuItemVistaAmbos;
        private System.Windows.Forms.BindingSource alumnosMayores21BindingSource;
        private System.Windows.Forms.TableLayoutPanel DistribucionTablaDepartamentos;
        private System.Windows.Forms.Panel PanelEdicionDepartamentos;
        private System.Windows.Forms.Button btnUltimoDepartamento;
        private System.Windows.Forms.Button btnPrimerDepartamento;
        private System.Windows.Forms.Button btnEliminarDepartamento;
        private System.Windows.Forms.Button btnAnadirDepartamento;
        private System.Windows.Forms.Label labelNumeroDepartamentos;
        private System.Windows.Forms.Button btnSiguienteDepartamento;
        private System.Windows.Forms.Button btnAnteriorDepartamento;
        private System.Windows.Forms.Label labelTituloDepartamentos;
        private System.Windows.Forms.DataGridView departamentosDataGridView;
        private System.Windows.Forms.BindingSource departamentosBindingSource;
        private educacionDataSetTableAdapters.DepartamentosTableAdapter departamentosTableAdapter;
        private System.Windows.Forms.TextBox idTextBoxDept;
        private System.Windows.Forms.TextBox nombreTextBox2;
        private System.Windows.Forms.TableLayoutPanel DistribucionTablaProfesores;
        private System.Windows.Forms.Panel PanelEdicionProfesores;
        private System.Windows.Forms.Button BtnUltimoProfesor;
        private System.Windows.Forms.Button BtnPrimerProfesor;
        private System.Windows.Forms.Button BtnEliminarProfesor;
        private System.Windows.Forms.Button BtnAnadirProfesor;
        private System.Windows.Forms.Label labelNumeroProfesores;
        private System.Windows.Forms.Button BtnSiguienteProfesor;
        private System.Windows.Forms.Button BtnAnteriorProfesor;
        private System.Windows.Forms.Label labelTituloProfesores;
        private System.Windows.Forms.DataGridView profesoresDataGridView;
        private System.Windows.Forms.BindingSource profesoresBindingSource;
        private educacionDataSetTableAdapters.ProfesoresTableAdapter profesoresTableAdapter;
        private System.Windows.Forms.TextBox dNITextBoxProf;
        private System.Windows.Forms.TextBox nombreTextBox3;
        private System.Windows.Forms.TextBox id_DepartamentoTextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id_Departamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.BindingSource profesoresDeptLenguaBindingSource;
        private educacionDataSetTableAdapters.ProfesoresDeptLenguaTableAdapter profesoresDeptLenguaTableAdapter;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem MenuItemConsultaEspecifica;
    }
}

