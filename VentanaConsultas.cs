﻿using System.Windows.Forms;

namespace AccessEducacion
{
    public partial class VentanaConsultas : Form
    {
        //
        // Constructor
        //
        public VentanaConsultas() : this(null) { }
        public VentanaConsultas(BindingSource res)
        {
            InitializeComponent();

            if (res != null)
                consultaDataGridView.DataSource = res;
            else
                consultaDataGridView.Visible = false;

        }
    }
}
